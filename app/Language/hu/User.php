<?php

return [
    'UserName' => 'Felhasználónév',
    'Email' => 'Email cím',
    'Password' => 'Jelszó',
    'ConfirmPassword' => 'Jelszó megerősítése',
];
