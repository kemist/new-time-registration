<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use Config\Services;

class User extends BaseController {

  protected $helpers = ['form'];

  public function index() {
    $validation = Services::validation();

    if (strtolower($this->request->getMethod()) !== 'post') {

      return view('user/index', [
          'validation' => $validation
      ]);
    }

    $validation->setRules($this->getFormValidationRules());

    if (!$validation->withRequest($this->request)->run()) {
      return view('user/index', [
          'validation' => $validation,
      ]);
    }

    $userModel = model(\App\Models\User::class);

    $userEntity = new \App\Entities\User();
    $userEntity->username = $this->request->getPost('username');
    $userEntity->email = $this->request->getPost('email');
    $userEntity->setPassword($this->request->getPost('password'));
    $userModel->save($userEntity);

    return redirect()->to(site_url("/"))->with('message','Sikeres regisztráció!');
  }

  /**
   * Mező(k) ellenőrzése Ajax-szal
   * @return string
   */
  public function validateField() {
    if ($this->request->isAJAX()) {
      $response = ['ok' => true];
      $validation = Services::validation();

      $validation->setRules($this->getFormValidationRules($this->request->getVar('field')));
      if (!$validation->withRequest($this->request)->run()) {
        $response = ['errors' => $validation->getErrors()];
      }
      return json_encode($response);
    }
  }

  /**
   * Űrlap validálási szabályok
   * 
   * @param string $fieldName
   * @return array
   */
  protected function getFormValidationRules($fieldName = '') {
    if (!is_array($fieldName) && $fieldName != '') {
      $fieldName = [$fieldName];
    }
    $rules = [
        'username' => [
            'label' => 'User.UserName',
            'rules' => 'required|is_unique[users.username]',
            'errors' => [
                'is_unique' => 'A megadott felhasználónév már foglalt!'
            ]
        ],
        'email' => [
            'label' => 'User.Email',
            'rules' => 'required|valid_email|is_unique[users.email]',
            'errors' => [
                'is_unique' => 'A megadott email cím már foglalt!'
            ]
        ],
        'password' => [
            'label' => 'User.Password',
            'rules' => 'required|min_length[7]|regex_match[(?=.*\d)(?=.*[a-z])(?=.*[A-Z])]',
            'errors' => [
                'regex_match' => 'A jelszónak tartalmaznia kell kisbetűt, nagybetűt és számot is!'
            ]
        ],
        'confirm-password' => [
            'label' => 'User.ConfirmPassword',
            'rules' => 'required|matches[password]',
        ],
    ];

    if (!empty($fieldName)) {
      $result = [];
      foreach ($fieldName as $field) {
        if (isset($rules[$field])) {
          $result[$field] = $rules[$field];
        }
      }
      return $result;
    }

    return $rules;
  }

}
