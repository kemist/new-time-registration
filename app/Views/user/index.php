<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>New Time - Regisztráció</title>
    <meta name="description" content="New Time - Regisztráció">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css'); ?>">


  </head>
  <body>

    <!-- HEADER: MENU + HEROE SECTION -->
    <header>



    </header>

    <!-- CONTENT -->

    <section class="signup w-100 m-auto p-2">

      <h1 class="h3 mb-5 text-center">New Time - Regisztráció</h1>

      <?php echo session()->getFlashdata('error') ?>
      <?php echo session()->getFlashdata('message') ?>
      <?php echo $validation->listErrors() ?>

      <?php if ($success): ?>
        <div class="alert alert-success" role="alert">
          Sikeres regisztráció!
        </div>
      <?php endif; ?>


      <?php echo form_open('/'); ?>
      <?php echo csrf_field(); ?>

      <div class="mb-3">
        <label for="username">Felhasználónév</label>
        <input 
          class="form-control" 
          type="text" 
          name="username" 
          id="username" 
          value="<?php echo set_value('username'); ?>" 
          size="50" 
          aria-describedby="ajax-error-username"
          required>
        <div id="ajax-error-username" class="ajax-error invalid-feedback"></div>
      </div>
      <div class="mb-3" >
        <label for="email">Email cím</label>
        <input 
          class="form-control" 
          type="email" 
          name="email" 
          id="email" 
          value="<?php echo set_value('email'); ?>" 
          size="50" 
          aria-describedby="ajax-error-email"
          required>
        <div id="ajax-error-email" class="ajax-error invalid-feedback"></div>
      </div>
      <div class="mb-3">
        <label for="password">Jelszó</label>
        <input 
          class="form-control" 
          type="password" 
          name="password" 
          id="password" 
          value="<?php echo set_value('password'); ?>" 
          size="50" 
          aria-describedby="ajax-error-password"
          required>
        <div id="ajax-error-password" class="ajax-error invalid-feedback"></div>
      </div>
      <div class="mb-3">
        <label for="confirm-password">Jelszó megerősítése</label>
        <input 
          class="form-control" 
          type="password" 
          name="confirm-password" 
          id="confirm-password" 
          value="<?php echo set_value('confirm-password'); ?>" 
          size="50" 
          aria-describedby="ajax-error-confirm-password"
          required>
        <div id="ajax-error-confirm-password" class="ajax-error invalid-feedback" ></div>
      </div>

      <div class="mb-3"><input type="submit" value="Küldés" class="btn btn-primary btn-lg"></div>

      <?php echo form_close(); ?>
    </section>

  </div>


  <footer>



  </footer>

  <!-- SCRIPTS -->
  <script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>

  <script src="<?php echo base_url('js/app.js'); ?>"></script>




</body>
</html>
