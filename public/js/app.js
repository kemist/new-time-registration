var NewTime = NewTime || {};

if (typeof jQuery !== "undefined") {

  $(document).ready(function () {

    $('#username').on('blur', function () {
      NewTime.validateField('username');
    });

    $('#email').on('blur', function () {
      NewTime.validateField('email');
    });

    $('#password').on('blur', function () {
      NewTime.validateField('password');
    });

    $('#confirm-password').on('blur', function () {
      NewTime.validateField(['confirm-password', 'password']);
    });

  });


  NewTime.validateField = function (fieldName) {
    if (typeof fieldName == 'string') {
      fieldName = [fieldName];
    }

    var data = '';
    for (var i = 0; i < fieldName.length; i++) {
      $('#ajax-error-' + fieldName[i]).html('');
      $('#' + fieldName[i]).removeClass('is-invalid').removeClass('is-valid');
      data += (data != '' ? '&' : '');
      data += 'field[]=' + escape(fieldName[i]) + '&' + escape(fieldName[i]) + '=' + escape($('#' + fieldName[i]).val())
    }
    $.ajax({
      type: 'GET',
      url: 'user/validateField',
      data: data,
      dataType: 'json',
      cache: false,
      async: true,
      contentType: 'application/x-www-form-urlencoded',
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(errorThrown + "\n" + XMLHttpRequest.responseText);
      },
      success: function (data, textStatus, XMLHttpRequest) {
        if (typeof data.errors !== 'undefined') {
          for (var i = 0; i < fieldName.length; i++) {
            if (typeof data.errors[fieldName[i]] !== 'undefined') {
              $('#ajax-error-' + fieldName[i]).append('<li>' + data.errors[fieldName[i]] + '</li>');
              $('#' + fieldName[i]).addClass('is-invalid');
            }
          }
        } else {
          for (var i = 0; i < fieldName.length; i++) {
            $('#' + fieldName[i]).addClass('is-valid');
          }
        }
      }
    });
  };
}